import { ref } from "vue";
import { defineStore } from "pinia";

import { LogElement } from "@/interfaces/global";

export const useLogStore = defineStore("log", () => {
  const errorsAmount = ref(0);
  const logList = ref<LogElement[]>([]);

  const clear = () => {
    errorsAmount.value = 0;
    logList.value = [];
  };

  const increaseErrors = () => {
    errorsAmount.value++;
  };

  const add = (logElement: LogElement) => {
    logList.value.push(logElement);
  };

  return {
    errorsAmount,
    logList,

    add,
    clear,
    increaseErrors,
  };
});
