import { computed, ref } from "vue";
import { defineStore } from "pinia";

import { ReleaseFileList, StatusData } from "@/interfaces/global";
import { Config, DownloadElement } from "@/interfaces/config";

import { BuildType, StatusProperty, FileListProperty, Status } from "@/constants/global";

import { useLogStore } from "./log";

const FileListDefaultValue: ReleaseFileList = {
  [FileListProperty.Config]: null,
  [FileListProperty.Screens]: [],
  [FileListProperty.Video]: null,
  [FileListProperty.Downloads]: [],
};

const StatusDataDefaultValue = {
  total: Status.NotStarted,
  config: Status.NotStarted,
  files: Status.NotStarted,
  build: Status.NotStarted,
};

export const useAppStore = defineStore("app", () => {
  const logStore = useLogStore();

  const decodedConfig = ref<Config | null>(null);

  const devMode = ref(false);
  const iconsAmount = ref(0);

  const fileNames = ref<string[]>([]);
  const fileList = ref<ReleaseFileList>({ ...FileListDefaultValue });
  const statusData = ref<StatusData>({ ...StatusDataDefaultValue });

  const folderName = ref("");
  const folderPath = ref("");
  const scormLink = ref("");

  const isMicro = computed(() => (decodedConfig.value ? decodedConfig.value.buildType === BuildType.Micro : false));

  const isFileChoosed = computed(() => statusData.value.total === Status.Preparing);
  const isInProgress = computed(() => statusData.value.total === Status.InProgress);

  const setStatus = (property: StatusProperty, value: Status) => {
    statusData.value[property] = value;
  };

  const setScormLink = (link: string) => {
    scormLink.value = link;
  };

  const setDevMode = (value: boolean) => {
    devMode.value = value;
  };

  const setFolderData = (path: string) => {
    folderPath.value = path;
    folderName.value = folderPath.value.split("\\").pop() || "";
    statusData.value.total = Status.Preparing;
  };

  const setFileNamesFromFolder = (folderContent: string[]) => {
    folderContent.forEach((file) => {
      fileNames.value.push(file);
    });
  };

  const setToFileList = (property: FileListProperty, value: any) => {
    fileList.value[property] = value;
  };

  const setIconsAmount = (amount: number) => {
    iconsAmount.value = amount;
  };

  const setConfig = (config: Config) => {
    decodedConfig.value = config;
  };

  const getConfigPath = () => {
    const configFile = fileNames.value.find((elem) => elem === "config.json");

    if (!configFile) {
      throw new Error();
    }

    fileList.value.config = configFile;
    return `${folderPath.value}\\${configFile}`;
  };

  // Очищение логов, ссылки на папку и скорм, статусов и счетчика ошибок
  const clearData = () => {
    logStore.clear();

    statusData.value = { ...StatusDataDefaultValue };
    fileList.value = { ...FileListDefaultValue };
    fileNames.value = [];
    folderName.value = "";
    folderPath.value = "";
    scormLink.value = "";
  };

  return {
    decodedConfig,
    devMode,
    isMicro,
    fileList,
    fileNames,
    folderName,
    folderPath,
    isFileChoosed,
    isInProgress,
    scormLink,
    statusData,

    clearData,
    setConfig,
    getConfigPath,
    setDevMode,
    setFileNamesFromFolder,
    setFolderData,
    setIconsAmount,
    setScormLink,
    setStatus,
    setToFileList,
  };
});
