import fs from "fs/promises";
import fsLegacy from "fs";
import converter from "xml-js";
import archiver from "archiver";
import VideoLib from "node-video-lib";
import * as utils from '@/js/utils';

export class TestFs {
  copy = async () => {
    const folderPath = utils.fixPath("C:/Users/Kaiser/WEB_Projects/work/CRMM/release-generator/test_examples/TextRelease_scrollhelp");
    const folderContent = await fs.readdir(folderPath, "utf8");
    console.warn(folderContent);
  }
}

export const testFs = new TestFs();

