import { useAppStore } from "@/stores/app";
import { useLogStore } from "@/stores/log";

import * as utils from "@/js/utils";

import { Config, SlideElement } from "@/interfaces/config";
import { LogName } from "@/interfaces/global";

import { Season, Status, FileListProperty, StatusProperty, MaxSCOtitleLength } from "@/constants/global";
import { LnError } from "@/constants/logNames";

import { Logger } from "./logger";
import { manager } from "./manager";

const downloadExt = ["pdf", "xls", "xlsx", "doc", "docx", "ppt", "pptx", "txt"];
const screenExt = ["jpg", "jpeg", "png"];
const reservedSeasons = [Season.Winter, Season.Spring, Season.Summer, Season.Autumn];

abstract class ConcreteValidator {
  logger: Logger;

  constructor() {
    const { logger } = manager.getClasses();
    this.logger = logger;
  }

  log(name: LogName, ...args: any[]) {
    this.logger.add(name, ...args);
  }

  validate(): void {}
}

class ConfigValidator extends ConcreteValidator {
  dc!: Config;
  appStore;
  logStore;

  constructor() {
    super();
    const { appStore, logStore } = manager.getClasses();

    this.appStore = appStore;
    this.logStore = logStore;

    this.dc = this.appStore.decodedConfig as Config;
  }

  validate() {
    this.appStore.setStatus(StatusProperty.Config, Status.InProgress);

    try {
      this.validateGeneral();
      this.validateSeason();
      this.validateQuestion();
      this.validateDownloads();
      this.validateManifest();
      this.validateAnketa();

      // Проверка текстового слайда. Только для Текстового релиза
      if (!this.appStore.isMicro) {
        this.validateTextslide();
      }

      this.validateKeys();

      if (this.logStore.errorsAmount) {
        throw new Error();
      } else {
        this.appStore.setStatus(StatusProperty.Config, Status.Completed);
      }
    } catch (e) {
      this.appStore.setStatus(StatusProperty.Total, Status.Failed);
      this.appStore.setStatus(StatusProperty.Config, Status.Failed);
    }
  }

  validateGeneral() {
    if (!this.dc.buildType) {
      this.log(LnError.EMPTY_BUILD_TYPE);
    }
    if (!this.dc.chapterName) {
      this.log(LnError.EMPTY_CHAPTER_NAME);
    }
    if (!this.dc.courseName) {
      this.log(LnError.EMPTY_COURSE_NAME);
    }
  }

  validateSeason() {
    if (!this.dc.season) {
      this.log(LnError.EMPTY_SEASON);
    } else if (!reservedSeasons.includes(this.dc.season.toLowerCase() as Season)) {
      this.log(LnError.INCORRECT_SEASON);
    }
  }

  validateTextslide() {
    if (!Array.isArray(this.dc.textSlide)) {
      this.log(LnError.ARRAY_TEXTSLIDE);
    }

    const textSlide = this.dc.textSlide as SlideElement[];

    textSlide.forEach((elem, elemId) => {
      // Текст
      if (elem.content) {
        if (!utils.isInnerTextCorrect(elem.content)) {
          this.log(LnError.INCORRECT_TEXT, elemId + 1);
        }
      }

      // Иконка
      if (elem.icon) {
        if (elem.hasOwnProperty("important")) {
          this.log(LnError.ICON_AND_IMPORTANT_CONFLICT, elemId + 1);
        }
        if (typeof elem.icon !== "string") {
          this.log(LnError.NOT_VALID_ICON, elemId + 1);
        }
        if (!utils.isIconExist(elem.icon)) {
          this.log(LnError.ICON_NOT_FOUND, elemId + 1, elem.icon);
        }
        if (!elem.align) {
          this.log(LnError.ICON_WITHOUT_ALIGN, elemId + 1);
        }
      }

      // Выравнивание
      if (elem.align) {
        if (!elem.align) {
          this.log(LnError.EMPTY_ALIGN, ++elemId);
        } else if (elem.align !== "left" && elem.align !== "center") {
          this.log(LnError.NOT_VALID_ALIGN, elemId + 1);
        }
        if (!elem.icon) {
          this.log(LnError.ALIGN_WITHOUT_ICON, elemId + 1);
        }
      }

      // Блок Важно
      if (elem.hasOwnProperty("important") && !elem.important) {
        this.log(LnError.NOT_VALID_IMPORTANT, elemId + 1);
      }

      // Скрины
      if (elem.screen) {
        if (typeof elem.screen !== "string") {
          this.log(LnError.NOT_VALID_SCREEN, elemId + 1);
        }
        if (!utils.isFileExist(this.appStore.folderPath, elem.screen)) {
          this.log(LnError.SCREEN_NOT_FOUND, elemId + 1, elem.screen);
        }
      }
    });
  }

  validateQuestion() {
    if (!this.dc.question.text) {
      this.log(LnError.EMPTY_QUESTION_TEXT);
    }

    if (!Array.isArray(this.dc.question.answers)) {
      this.log(LnError.ARRAY_ANSWERS);
    }

    if (!this.dc.question.answers.some((answer) => answer.right)) {
      this.log(LnError.NO_RIGHT_ANSWER);
    }

    this.dc.question.answers.forEach((answer, answerId) => {
      if (!answer.text) {
        this.log(LnError.EMPTY_ANSWER_TEXT, answerId + 1);
      }
    });
  }

  validateDownloads() {
    if (!Array.isArray(this.dc.downloads)) {
      this.log(LnError.ARRAY_DOWNLOADS);
    }

    this.dc.downloads.forEach((download, downloadId) => {
      const id = downloadId + 1;

      if (!download.url) {
        this.log(LnError.EMPTY_DOWNLOADS_URL, id);
      } else if (!download.title) {
        this.log(LnError.EMPTY_DOWNLOADS_TITLE, id);
      }
      if (!utils.checkFileName(download.url)) {
        this.log(LnError.NOT_VALID_DOWNLOAD_CONFIG, id);
      }
    });
  }

  validateManifest() {
    if (!this.dc.manifest.manifestId) {
      this.log(LnError.EMPTY_MANIFEST_ID);
    }
    if (!this.dc.manifest.SCOtitle) {
      this.log(LnError.EMPTY_SCO_TITLE);
    }
    if (this.dc.manifest.SCOtitle.length > MaxSCOtitleLength) {
      this.log(LnError.OVERFLOW_COURSE_NAME, this.dc.manifest.SCOtitle.length);
    }
    if (!this.dc.manifest.letter) {
      this.log(LnError.EMPTY_LETTER);
    }
  }

  validateAnketa() {
    if (this.dc.options?.anketa) {
      if (this.dc.options.anketa !== true) {
        this.log(LnError.OPTIONS_INCORRECT_ANKETA);
      }
    }
  }

  validateKeys() {
    const reservedKeys = ["buildType", "courseName", "chapterName", "season", "question", "downloads", "manifest"];
    let incorrectKeysLogName = LnError.M_INCORRECT_KEYS;

    if (this.appStore.isMicro) {
      // this.dc.video = "";
    } else {
      reservedKeys.push("textSlide");
      incorrectKeysLogName = LnError.T_INCORRECT_KEYS;
    }

    for (let reserved of reservedKeys) {
      if (!this.dc.hasOwnProperty(reserved)) {
        this.log(incorrectKeysLogName);
        break;
      }
    }
  }
}

class FilesValidator extends ConcreteValidator {
  dc!: Config;
  appStore;
  logStore;

  constructor() {
    super();
    this.appStore = useAppStore();
    this.logStore = useLogStore();
    this.dc = this.appStore.decodedConfig as Config;
  }

  validate() {
    this.appStore.setStatus(StatusProperty.Files, Status.InProgress);

    try {
      if (this.appStore.isMicro) {
        this.validateVideo();
      } else {
        this.validateScreens();
      }

      this.validateDownloads();

      if (this.logStore.errorsAmount) {
        throw new Error();
      } else {
        this.appStore.setStatus(StatusProperty.Files, Status.Completed);
      }
    } catch (e) {
      this.appStore.setStatus(StatusProperty.Total, Status.Failed);
      this.appStore.setStatus(StatusProperty.Files, Status.Failed);
    }
  }

  validateVideo() {
    const videofileNames = this.appStore.fileNames.filter((elem: string) => elem.includes("mp4"));
    this.appStore.setToFileList(FileListProperty.Video, videofileNames[0]);

    if (videofileNames.length > 1) {
      this.log(LnError.TOO_MANY_VIDEO);
    } else if (videofileNames.length === 0) {
      this.log(LnError.VIDEO_NOT_FOUND);
    } else if (videofileNames.length === 1) {
      const videofile = videofileNames[0];

      if (!videofile.includes(".mp4")) {
        this.log(LnError.NOT_MP4);
      }

      if (videofile.split(".").length > 2) {
        this.log(LnError.NOT_VALID_VIDEO_FILE);
      } else {
        if (!utils.checkFileName(videofile)) {
          this.log(LnError.NOT_VALID_VIDEO_FILE);
        }

        if (utils.checkOverflow(videofile)) {
          this.log(LnError.OVERFLOW_FILE_NAME, videofile);
        }
      }
    }
  }

  validateScreens() {
    const screenNames = this.appStore.fileNames.filter((elem: string) => {
      const extension = elem.split(".").pop() as string;
      return screenExt.includes(extension);
    });
    this.appStore.setToFileList(FileListProperty.Screens, screenNames);
  }

  validateDownloads() {
    const downloads = this.appStore.fileNames.filter((elem: string) => {
      const extension = elem.split(".").pop() as string;
      return downloadExt.includes(extension);
    });

    this.appStore.setToFileList(FileListProperty.Downloads, downloads);

    if (downloads.length !== this.dc.downloads.length) {
      this.log(LnError.DOWNLOADS_CONFLICT);
    }

    downloads.forEach((elem) => {
      if (!utils.checkFileName(elem)) {
        this.log(LnError.NOT_VALID_DOWNLOAD_FILE, elem);
      }
      if (utils.checkOverflow(elem)) {
        this.log(LnError.OVERFLOW_FILE_NAME, elem);
      }
    });

    this.dc.downloads.forEach((elem) => {
      if (!this.appStore.fileNames.includes(elem.url)) {
        this.log(LnError.DOWNLOAD_NOT_FOUND, elem.url);
      }
    });
  }
}

export class Validator {
  configValidator: ConfigValidator;
  filesValidator: FilesValidator;
  logger: Logger;

  constructor() {
    const { logger } = manager.getClasses();
    this.logger = logger;
    this.configValidator = new ConfigValidator();
    this.filesValidator = new FilesValidator();
  }

  validateConfig() {
    try {
      this.configValidator.validate();
      this.filesValidator.validate();
    } catch (err) {
      console.warn(err);
    }
  }
}
