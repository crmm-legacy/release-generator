// import fs from "fs-extra";
import fs from "fs/promises";
// import fsLegacy from "fs";
import converter from "xml-js";
import archiver from "archiver";
import VideoLib from "node-video-lib";
// const VideoLib = require("node-video-lib");

import * as utils from "@/js/utils";

import { Config } from "@/interfaces/config";

import {
  TempManifestPath,
  TempPath,
  FolderMicro,
  FolderText,
  SiteUrl,
  Status,
  IconsPath,
  BoilerplatePath,
  StatusProperty,
  TempConfigPath,
} from "@/constants/global";
import { LnError } from "@/constants/logNames";

import { Logger } from "./logger";
import { Validator } from "./validator";
import { manager } from "./manager";
import path from "path";

const BaseTagProps = {
  name: "file",
  type: "element",
};

export class FsManager {
  appStore;
  logStore;
  validator!: Validator;
  logger!: Logger;
  decoder!: number;
  dc!: Config;

  constructor() {
    const { appStore, logStore, validator, logger } = manager.getClasses();

    this.appStore = appStore;
    this.logStore = logStore;
    this.validator = validator;
    this.logger = logger;

    this.dc = this.appStore.decodedConfig as Config;
  }

  getFiles = async (filePaths: string[]) => {
    this.appStore.clearData();
    if (filePaths === undefined) {
      return false;
    }

    const folderPath = filePaths[0];
    console.warn(folderPath)
    const folderContent = await fs.readdir(folderPath, "utf8");

    this.appStore.setFolderData(folderPath);
    this.appStore.setFileNamesFromFolder(folderContent);
  };

  copyFromTo = async (sourcePath: string, destPath: string) => {
    await fs.cp(sourcePath, destPath, { recursive: true });
  };

  getManifest = async () => {
    const options = { ignoreComment: true, spaces: 2 };
    const xml = await fs.readFile(TempManifestPath, "utf8");
    const resJson = converter.xml2json(xml, options);
    const prsJson = JSON.parse(resJson);

    this.overwriteManifest(prsJson);

    const resultXml = converter.json2xml(JSON.stringify(prsJson), options);

    try {
      await fs.writeFile(TempManifestPath, resultXml);
      console.warn("The file was saved!");
    } catch (err) {
      return console.warn(err);
    }
  };

  // Взятие конфига из папки
  getData = async (configPath: string) => {
    try {
      const data = await fs.readFile(configPath, "utf8");

      try {
        return JSON.parse(data);
      } catch (e) {
        this.logger.add(LnError.FATAL_ERROR);
        this.appStore.setStatus(StatusProperty.Total, Status.Failed);
      }
    } catch (err) {
      this.logger.add(LnError.CONFIG_NOT_FOUND);
      this.appStore.setStatus(StatusProperty.Total, Status.Failed);
    }
  };

  overwriteManifest = (prsJson: any) => {
    const { manifest } = this.dc;
    const dateId = utils.createDateId(manifest.letter);
    const devInfo = utils.createDevInfo(utils.createDateWithDivider("."));

    // base == <manifest>
    const base = prsJson.elements[0];
    base.attributes.identifier = manifest.manifestId;
    base.elements[1].attributes.default = manifest.courseId;

    // organization == <organization>
    const organization = base.elements[1].elements[0];
    organization.attributes.identifier = manifest.courseId;
    organization.elements[0].elements[0].text = `${manifest.SCOtitle} (${dateId})`;
    organization.elements[1].attributes.identifierref = dateId;
    organization.elements[1].elements[0].elements[0].text = `${manifest.SCOtitle} (${dateId})`;

    organization.elements[2].elements[0].elements[0].elements[0].elements[0].elements[0].text = devInfo;

    // resources == <resources>
    const resources = base.elements[2].elements[0];
    resources.attributes.identifier = dateId;
    resources.elements[0].elements[0].elements[0].elements[0].elements[0].elements[0].text = `${manifest.SCOtitle} (${dateId})`;

    if (this.appStore.isMicro) {
      resources.elements.push({
        ...BaseTagProps,
        attributes: {
          href: `video/${this.appStore.fileList.video}`,
        },
      });
    }

    this.dc.downloads.forEach((elem: any) => {
      resources.elements.push({
        ...BaseTagProps,
        attributes: {
          href: `downloads/${elem.url}`,
        },
      });
    });

    resources.elements.push({
      ...BaseTagProps,
      attributes: {
        href: `config.json`,
      },
    });
  };

  openVideo = async (videoPath: string) => {
    const fd = await fs.open(videoPath, "r");
    // this.decoder = fd;
  };

  getDuration = async () => {
    const movie = VideoLib.MovieParser.parse(this.decoder);
    const secString = Math.floor(movie.relativeDuration()).toString();
    const duration = utils.toHHMMSS(secString);

    return duration;
  };

  replaceConfig = async () => {
    if (!this.appStore.isMicro) {
      return;
    }

    const tempPath = `${this.appStore.folderPath}\\${this.appStore.fileList.video}`;
    const videoPath = tempPath.replace(/\\/g, "/");

    await this.openVideo(videoPath);
    const duration = await this.getDuration();

    this.dc.video = {
      duration,
      src: this.appStore.fileList.video as string,
    };
  };

  replaceFiles = async () => {
    const { folderPath, fileList } = this.appStore;
    const promiseArr = [];

    fileList.downloads.forEach((download) => {
      promiseArr.push(async () => {
        const sourcePath = utils.fixPath(`${folderPath}\\${download}`);
        const destPath = `./sandbox/temp/downloads/${download}`;
        await this.copyFromTo(sourcePath, destPath);
      });
    });

    fileList.downloads.forEach((download) => {
      promiseArr.push(async () => {
        const sourcePath = utils.fixPath(`${folderPath}\\${download}`);
        const destPath = `./sandbox/temp/downloads/${download}`;
        await this.copyFromTo(sourcePath, destPath);
      });
    });

    if (this.appStore.isMicro) {
      promiseArr.push(async () => {
        const sourcePath = utils.fixPath(`${folderPath}\\${fileList.video}`);
        const destPath = `./sandbox/temp/video/${fileList.video}`;
        await this.copyFromTo(sourcePath, destPath);
      });
    }

    if (!this.appStore.isMicro) {
      if (fileList.screens) {
        fileList.screens.forEach((screen) => {
          promiseArr.push(async () => {
            const sourcePath = utils.fixPath(`${folderPath}\\${screen}`);
            const destPath = `./sandbox/temp/img/slide-screens/${screen}`;
            await this.copyFromTo(sourcePath, destPath);
          });
        });
      }
    }

    promiseArr.push(async () => {
      const modConfig = JSON.stringify(this.dc, null, 4);
      await fs.writeFile(TempConfigPath, modConfig);
    });

    await Promise.all(promiseArr);
  };

  copyBoilerplate = async () => {
    try {
      await fs.cp(BoilerplatePath, TempPath, { recursive: true });
    } catch (err) {
      console.error(err);
    }
  };

  createScorm = async () => {
    // return new Promise(async (resolve, reject) => {
    //   const manifest = this.dc.manifest;
    //   const zipName = utils.createZipName(manifest);
    //   const targetDirectory = `./scorm/${zipName}`;

    //   try {
    //     await fs.stat("./scorm/");
    //   } catch (err) {
    //     reject();
    //   }

    //   const output = fsLegacy.createWriteStream(targetDirectory);
    //   const archive = archiver("zip", { zlib: { level: 9 } });

    //   archive.pipe(output);
    //   archive.directory(TempPath, false);

    //   output.on("close", resolve);

    //   archive.on("warning", (err) => {
    //     if (err.code !== "ENOENT") throw err;
    //   });

    //   archive.finalize();
    // });
  };

  // Асинхронно копируем бойлерплейт, подмешиваем туда файлы, модифицируем манифест
  // архивируем в .zip и очищаем временную папку
  async buildScorm() {
    this.appStore.setStatus(StatusProperty.Build, Status.InProgress);

    try {
      await this.copyBoilerplate();
      await this.replaceConfig();
      await this.replaceFiles();
      await this.getManifest();
      await this.createScorm();
      await this.clearTemp();
      this.setScormLink();

      this.appStore.setStatus(StatusProperty.Build, Status.Completed);
    } catch (err) {
      this.appStore.setStatus(StatusProperty.Build, Status.Failed);
      throw err;
    }
  }

  clearTemp = async () => {
    const directory = TempPath;

    for (const file of await fs.readdir(directory)) {
      await fs.unlink(path.join(directory, file));
    }
  };

  setScormLink = () => {
    if (!this.appStore.decodedConfig) {
      return;
    }

    const targetFolder = this.appStore.isMicro ? FolderMicro : FolderText;
    const date = utils.createDateWithDivider("_", true);
    const zipName = utils.createZipName(this.appStore.decodedConfig.manifest);
    const scormLink = `${SiteUrl}${targetFolder}/${date}/${zipName}`;

    this.appStore.setScormLink(scormLink);
  };

  countSlideIcons = async () => {
    const files = await fs.readdir(IconsPath);
    this.appStore.setIconsAmount(files.length);
  };
}
