import { useAppStore } from "@/stores/app";
import { useLogStore } from "@/stores/log";

import { Status, StatusProperty } from '@/constants/global';
import { LnCommon, LnError, LnSuccess } from "@/constants/logNames";

import { FsManager } from "./fsManager";
import { Logger } from "./logger";
import { Validator } from "./validator";
import { Key } from '../constants/global';

export class Manager {
  appStore!: ReturnType<typeof useAppStore>;
  logStore!: ReturnType<typeof useLogStore>;
  fsManager!: FsManager;
  validator!: Validator;
  logger!: Logger;

  constructor() {}

  init() {
    this.appStore = useAppStore();
    this.logStore = useLogStore();

    this.fsManager = new FsManager();
    this.validator = new Validator();
    this.logger = new Logger();
  }

  getClasses() {
    return {
      appStore: this.appStore,
      logStore: this.logStore,
      fsManager: this.fsManager,
      validator: this.validator,
      logger: this.logger,
    }
  }

  startWork = async () => {
    this.appStore.clearData();
    this.appStore.setStatus(StatusProperty.Total, Status.InProgress);
    
    const configPath = this.appStore.getConfigPath();
    const result = await this.fsManager.getData(configPath);

    this.appStore.setConfig(result);
    this.logger.add(LnCommon.BUILD_NAME, this.appStore.isMicro ? "МИКРОРЕЛИЗ" : "ТЕКСТОВЫЙ РЕЛИЗ");
    await this.fsManager.countSlideIcons();
    this.validator.validateConfig();

    try {
      await this.fsManager.buildScorm();
      this.finishWork();
    } catch (err) {
      console.warn("!!!!!!!! ERROR", err)
    }
  }

  finishWork = () => {
    if (this.logStore.errorsAmount) {
      this.logger.add(LnError.BUILD_FAILED);
      this.appStore.setStatus(StatusProperty.Total, Status.Failed);
    } else {
      this.logger.add(LnSuccess.BUILD_COMPLETED);
      this.appStore.setStatus(StatusProperty.Total, Status.Completed);
    }
  };

  closeApp = () => {
    window.close();
  };

  setEvents = () => {
    let mCount = 0;

    document.onkeydown = (e) => {
      e.preventDefault();

      if (e.code === Key.M && e.ctrlKey && mCount >= 1) {
        this.appStore.setDevMode(!this.appStore.devMode);
        mCount = 0;
      } else if (e.code === Key.M) {
        mCount++;
      }
    };
  };
}

export const manager = new Manager();
