import { error, warning, common, success } from "../js/messages";

import { LogName } from "@/interfaces/global";

import { LogType } from "@/constants/global";
import { LnError, LnCommon, LnSuccess, LnWarning } from "@/constants/logNames";

import { manager } from "./manager";

type Namespace = {
  [s: string]: LogName;
};

const hasName = (namespace: Namespace, name: LogName) => Object.keys(namespace).includes(name);

export class Logger {
  logStore;

  constructor() {
    const { logStore } = manager.getClasses();

    this.logStore = logStore;
  }

  add = (name: LogName, ...args: any[]) => {
    let type = LogType.Common;
    let message: string | Function = "";

    if (hasName(LnError, name)) {
      this.logStore.increaseErrors();
      type = LogType.Error;
      message = error[name as LnError];
    } else if (hasName(LnWarning, name)) {
      type = LogType.Warning;
      message = warning[name as LnWarning];
    } else if (hasName(LnSuccess, name)) {
      type = LogType.Success;
      message = success[name as LnSuccess];
    } else {
      message = common[name as LnCommon];
    }

    this.logStore.add({
      message: typeof message === "string" ? message : message(),
      type,
    });
  };
}
