export enum Status {
  NotStarted,
  Preparing,
  InProgress,
  Failed,
  Completed,
}

export enum LogType {
  Common,
  Warning,
  Error,
  Success,
}

export enum BuildType {
  Micro = "micro",
  Text = "text",
}

export enum Season {
  Winter = "winter",
  Spring = "spring",
  Summer = "summer",
  Autumn = "autumn",
}

export enum FileListProperty {
  Config = "config",
  Screens = "screens",
  Video = "video",
  Downloads = "downloads",
}

export enum StatusProperty {
  Total = "total",
  Config = "config",
  Files = "files",
  Build = "build",
}

export const Meta = {
  provider: 'ООО "Синерджи Технолоджи"',
  technology: "html5",
  environment: "Собственный движок",
};

export const TempManifestPath = "./sandbox/temp/imsmanifest.xml";
export const TempConfigPath = "./sandbox/temp/config.json";
export const BoilerplatePath = "./sandbox/boilerplate";
export const IconsPath = "./sandbox/boilerplate/img/slide-icon";
export const TempPath = "./sandbox/temp";
export const SiteUrl = "https://synergycorp.ru/u/";
export const FolderMicro = "sb_microrelease_HTML";
export const FolderText = "sb_textrelease_HTML";

export const MaxSCOtitleLength = 89;
export const MaxFileNameLength = 64;

export const Key = {
  M: "KeyM"
};
