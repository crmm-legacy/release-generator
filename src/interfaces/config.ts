import { BuildType, Season } from "../constants/global";

export interface DownloadElement {
  title: string;
  url: string;
}

export interface SlideElement {
  icon: number;
  align?: string;
  important?: true;
  screen?: string;
  content: string;
}

interface Answer {
  text: string;
  right?: boolean;
}

interface Question {
  text: string;
  answers: Answer[];
}

export interface Manifest {
  courseId: string;
  manifestId: string;
  SCOtitle: string;
  letter: string;
}

export interface Config {
  buildType: BuildType;
  courseName: string;
  chapterName: string;
  season: Season;
  question: Question;
  downloads: DownloadElement[];
  manifest: Manifest;
  video?: {
    duration: string;
    src: string;
  };
  textSlide?: SlideElement[];
  options?: {
    anketa?: true;
  };
}
