import { LogType, Status } from "../constants/global";
import { LnError, LnSuccess, LnCommon, LnWarning } from '../constants/logNames';

export interface ReleaseFileList {
  config: string | null,
  video: string | null,
  downloads: string[],
  screens: string[],
}

export interface LogElement {
  message: string;
  type: LogType;
}

export interface StatusData {
  total: Status;
  config: Status;
  files: Status;
  build: Status;
}

export type LogName = LnError | LnSuccess | LnWarning | LnCommon;
