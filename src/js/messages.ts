import { LnCommon, LnError, LnSuccess, LnWarning } from "../constants/logNames";

export const error = {
  // Структура
  [LnError.EMPTY_BUILD_TYPE]: "Не указан тип курса. Заполните поле buildType. Возможные значения - micro, text",
  [LnError.EMPTY_CHAPTER_NAME]: "Не указано название раздела. Заполните поле chapterName",
  [LnError.EMPTY_COURSE_NAME]: "Не указано название курса. Заполните поле courseName",

  // Видео
  [LnError.NOT_MP4]: "Видеофайл должен иметь формат .mp4",
  [LnError.TOO_MANY_VIDEO]: "Найдено более одного видеофайла в указанной папке",
  [LnError.NOT_VALID_VIDEO_FILE]: "Видеофайл содержит посторонние символы в названии",
  [LnError.VIDEO_NOT_FOUND]: "Не удалось найти видеофайл в указанной папке",
  [LnError.T_INCLUDES_VIDEO]: 'Обнаружен видеофайл в целевой папке. Курсы типа "text" не должны содержать видео',

  // Текстовый слайд
  [LnError.ARRAY_TEXTSLIDE]: "Поле textSlide должно быть массивом",
  [LnError.INCORRECT_TEXT]: (number: number) =>
    `textSlide. Несовпадает количество открывающих и закрывающих тегов b, i в элементе №${number}`,
  [LnError.ICON_AND_IMPORTANT_CONFLICT]: (number: number) =>
    `textSlide. Одновременное использование иконки и блока "Важно" в элементе №${number}. Разрешено использовать только один из вариантов`,
  [LnError.NOT_VALID_IMPORTANT]: (number: number) =>
    `textSlide. Параметр important в элементе №${number} имеет некорректное значение. Укажите "important": true, если данный элемент является блоком "Важно", либо удалите параметр`,
  [LnError.NOT_VALID_ICON]: (number: number) =>
    `textSlide. Параметр icon в элементе №${number} имеет некорректное значение. Укажите название иконки в текстовом формате`,
  [LnError.ICON_NOT_FOUND]: (number: number, icon: string) =>
    `textSlide. Не найдена иконка ${icon}, указанная в элементе №${number}. Проверьте название указанной иконки`,
  [LnError.ICON_WITHOUT_ALIGN]: (number: number) =>
    `textSlide. Отсутствует параметр align в элементе №${number}. При использовании параметра icon обязательно должен быть указан параметр align`,
  [LnError.NOT_VALID_SCREEN]: (number: number) =>
    `textSlide. Параметр screen в элементе №${number} имеет некорректное значение. Укажите название изображения в текстовом формате`,
  [LnError.SCREEN_NOT_FOUND]: (number: number, screen: string) =>
    `textSlide. Не найдено изображение ${screen}, указанное в элементе №${number}. Проверьте название указанного изображения`,
  [LnError.NOT_VALID_ALIGN]: (number: number) =>
    `textSlide. Параметр align в элементе №${number} имеет некорректное значение. Допустимые значения: left, center`,
  [LnError.EMPTY_ALIGN]: (number: number) =>
    `textSlide. Не указан параметр align в элементе №${number}. Допустимые значения: left, center`,
  [LnError.ALIGN_WITHOUT_ICON]: (number: number) =>
    `textSlide. Отсутствует параметр icon в элементе №${number}. При использовании параметра align обязательно должен быть указан параметр icon`,

  // Вопрос
  [LnError.EMPTY_QUESTION_TEXT]: "Не указан текст вопроса. Заполните поле question.text",
  [LnError.ARRAY_ANSWERS]: "Поле question.answers должно быть массивом",
  [LnError.NO_RIGHT_ANSWER]:
    'Не установлен правильный ответ в тестировании. Добавьте "right": true к одному из ответов',
  [LnError.EMPTY_ANSWER_TEXT]: (number: number) =>
    `Не указан текст в ответе №${number}. Проверьте поля question.answers.text`,

  // Материалы на скачивание
  [LnError.ARRAY_DOWNLOADS]: "Поле downloads должно быть массивом",
  [LnError.EMPTY_DOWNLOADS_TITLE]: (number: number) => `Не указано название материала на скачивание №${number}`,
  [LnError.EMPTY_DOWNLOADS_URL]: (number: number) => `Не указана ссылка материала на скачивание №${number}`,
  [LnError.NOT_VALID_DOWNLOAD_FILE]: (name: string) =>
    `Материал на скачивание ${name} содержит посторонние символы в названии`,
  [LnError.NOT_VALID_DOWNLOAD_CONFIG]: (number: number) =>
    `Материал на скачивание №${number} в файле конфигурации содержит посторонние символы в названии. Проверьте поле url у материала на скачивание`,
  [LnError.DOWNLOAD_NOT_FOUND]: (name: string) => `Не найден материал на скачивание ${name}, указанный в config.json`,
  [LnError.DOWNLOADS_CONFLICT]: "Не совпадает количество материалов на скачивание в папке и config.json",

  // Манифест
  [LnError.EMPTY_COURSE_ID]: "Не указан id курса. Заполните поле manifest.courseId",
  [LnError.INCORRECT_COURSE_ID]: "Некорректный id курса. Id курса должно содержать подстроку vid_",
  [LnError.EMPTY_MANIFEST_ID]: "Не указан id манифеста. Заполните поле manifest.manifestId",
  [LnError.EMPTY_SCO_TITLE]: "Не указано название курса. Заполните поле manifest.SCOtitle",
  [LnError.OVERFLOW_COURSE_NAME]: (amount: number) =>
    `Название курса превышает допустимое значение. Разрешено использовать не более 89 символов в поле manifest.SCOtitle.<br>Сейчас: ${amount} символов`,
  [LnError.EMPTY_LETTER]:
    "Не указана версия сборки. Заполните поле manifest.letter или оставьте стандартное значение A",

  // Прочее
  [LnError.CONFIG_NOT_FOUND]:
    "Не найден файл конфигурации курса. Убедитесь, что в указанной папке содержится файл config.json",
  [LnError.EMPTY_SEASON]: "Не указано время года. Заполните поле season",
  [LnError.INCORRECT_SEASON]: "Некорректное время года. Допустимые значения: winter, spring, summer, autumn",
  [LnError.M_INCORRECT_KEYS]:
    "Файл конфигурации должен обязательно содержать параметры buildType, courseName, chapterName, season, question, downloads, manifest",
  [LnError.T_INCORRECT_KEYS]:
    "Файл конфигурации должен обязательно содержать параметры buildType, courseName, chapterName, season, textSlide, question, downloads, manifest",
  [LnError.OVERFLOW_FILE_NAME]: (name: string) =>
    `Разрешено использовать не более 64 символов для названий файлов (включая формат). Ограничьте название ${name}`,
  [LnError.OPTIONS_INCORRECT_ANKETA]:
    'Некорректное значение параметра options.anketa. Укажите "anketa": true, если в сборке должна присутствовать анкета, либо удалите параметр',
  [LnError.BUILD_FAILED]:
    "Не удалось опубликовать курс. Попробуйте опубликовать другую папку. Если ошибка повторяется, обратитесь к разработчику",
  [LnError.FATAL_ERROR]:
    "Неизвестная ошибка. Проверьте корректность файла config.json (присутствуют все запятые в массивах/объектах, проставлены открывающие/закрывающие кавычки, не используются двойные кавычки или посторонние символы внутри текстовых элементов)",
};

export const warning = {
  [LnWarning.ATTENTION]: "Предупреждение",
};

export const success = {
  [LnSuccess.BUILD_COMPLETED]: "Курс успешно опубликован",
};

export const common = {
  [LnCommon.BUILD_NAME]: (type: string) => type,
};
