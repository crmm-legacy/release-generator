import fs from "fs/promises";

import { Meta, IconsPath, MaxFileNameLength } from '../constants/global';
import { Manifest } from "../interfaces/config";

const n = (n: number) => (n > 9 ? `${n}` : `0${n}`);

export const createDevInfo = (buildDate: string) =>
  `Провайдер: ${Meta.provider}; Дата сборки: ${buildDate}; Технология: ${Meta.technology}; Среда разработки: ${Meta.environment}`;

export const createZipName = (manifest: Manifest) =>
  manifest.courseId ? `${manifest.manifestId}_${manifest.courseId}.zip` : `${manifest.manifestId}.zip`;

export const checkFileName = (fileName: string) => /^[a-zA-Z0-9_-]*$/.test(fileName.split(".")[0]);

export const isIconExist = async (iconName: number) => await fs.access(`${IconsPath}/${iconName}.png`);

export const isFileExist = async (folderPath: string, fileName: string) => await fs.access(`${folderPath}/${fileName}`);

export const checkOverflow = (fileName: string) => fileName.length > MaxFileNameLength;

export const createDateId = (letter: string) => {
  const currentDate = new Date();
  const dateString =
    n(currentDate.getDate()) + n(currentDate.getMonth() + 1) + currentDate.getFullYear().toString().substr(-2);

  return `${letter}_${dateString}`;
};

export const createDateWithDivider = (divider: string, isReverse?: boolean) => {
  const currentDate = new Date();
  const year = currentDate.getFullYear();
  const month = n(currentDate.getMonth() + 1);
  const date = n(currentDate.getDate());

  const dateString = isReverse ? year + divider + month + divider + date : date + divider + month + divider + year;

  return dateString;
};

export const isInnerTextCorrect = (text: string) => {
  let isCorrect = true;
  const targetTags = [
    ["<b>", "</b>"],
    ["<i>", "</i>"],
  ];

  targetTags.forEach((tagPair) => {
    if (text.indexOf(tagPair[0]) !== -1) {
      const regexpFirst = new RegExp(tagPair[0], "gi");
      const regexpSecond = new RegExp(tagPair[1], "gi");
      const firstLength = text.match(regexpFirst)?.length || 0;
      const secondLength = text.match(regexpSecond)?.length || 0;

      if (firstLength !== secondLength) {
        isCorrect = false;
      }
    }
  });

  return isCorrect;
};

export const toHHMMSS = (secs: string) => {
  const secNum = parseInt(secs, 10);
  const hours = Math.floor(secNum / 3600) % 24;
  const minutes = Math.floor(secNum / 60) % 60;
  const seconds = secNum % 60;

  return [hours, minutes, seconds].map((v) => (v < 10 ? `0${v}` : v)).join(":");
};

export const fixPath = (path: string) => path.replace(/\\/g, "/");
